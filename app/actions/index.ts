import Counterparty from "../models/Counterparty"
import { ActionCreator, Action, ActionCreatorsMapObject } from "redux"

interface CounterpartyAction extends Action {
  counterparty: Counterparty;
}

export const addCounterparty: ActionCreator<CounterpartyAction> = (counterparty: Counterparty) => ({
  type: "ADD_COUNTERPARTY",
  counterparty,
})

export const deleteCounterparty: ActionCreator<CounterpartyAction> = (counterparty: Counterparty) => ({
  type: "DEL_COUNTERPARTY",
  counterparty,
})

export const counterpartyActionsMap: ActionCreatorsMapObject = {
    "ADD_COUNTERPARTY": addCounterparty,
    "DEL_COUNTERPARTY": deleteCounterparty
}

export default counterpartyActionsMap;