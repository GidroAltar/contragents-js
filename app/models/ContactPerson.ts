
class ContactPerson {
    id: number;
    fullName: string;
    phone: string;
    email: string;
}

export default ContactPerson;