import AddressType from "./AddressType";

class Address {
    id: number;
    address: string;
    addressType: AddressType;
}

export default Address;