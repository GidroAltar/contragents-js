import * as React from "react";
import ContragentEditorPanelItem from "./contragent-editor-panel-item";


interface IState {
    title: string;
}

class ContragentEditorAddressPanel extends React.Component<any, IState> {

    state: IState = {
        title: "Алхимов А.А.",
    };

    render() {
        return (
                <div className="panel panel-default panel-contragent">
                    <div className="panel-heading">
                        <h4 className="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAddresses">
                                Адреса
                            </a>
                        </h4>
                    </div>
                    <div id="collapseAddresses" className="panel-collapse collapse">
                        <div className="panel-body">
                            <div className="well">
                                <div className="form-group">
                                    <label htmlFor="newAddressName">Название адреса:</label>
                                    <input type="text" className="form-control" id="newAddressName" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="newAddressValue">Значение адреса:</label>
                                    <input type="text" className="form-control" id="newAddressValue" />
                                </div>
                                <button type="button" className="btn btn-primary">Добавить</button>
                            </div>
                            <div className="panel-group">
                                <ContragentEditorPanelItem />
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

export default ContragentEditorAddressPanel;