import * as React from "react";

interface IState {
    title: string;
}

class ContragentListRow extends React.Component<any, IState> {

    render() {
        return (
            <tr>
                <td>Иванов</td>
                <td>ООО</td>
                <td>Алхимов А.А.</td>
                <td>
                    <a href="#">
                        <span className="glyphicon glyphicon-star"></span>
                    </a>
                </td>
                <td>
                    <a href="#" className="btn btn-info btn-sm">
                        <span className="glyphicon glyphicon-pencil"></span>
                    </a>
                    <a href="#" className="btn btn-info btn-sm">
                        <span className="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        );
    }
}

export default ContragentListRow;
