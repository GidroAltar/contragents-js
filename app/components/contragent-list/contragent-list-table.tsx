 import * as React from "react";
import ContragentListRow from "./contragent-list-row"

interface IState {
    title: string;
}

class ContragentListTable extends React.Component<any, IState> {

    render() {
        return (
           <div className="table-responsive">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Наименование</th>
                        <th>Тип юридического лица</th>
                        <th>Ответственный</th>
                        <th>
                            <span>
                                <span className="glyphicon glyphicon-star"></span>
                            </span>
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <ContragentListRow />
                </tbody>
            </table>
        </div>
        );
    }
}

export default ContragentListTable;