import * as React from "react"
import { connect } from "react-redux"
import { bindActionCreators, ActionCreatorsMapObject } from "redux"

import ContragentList from "./contragent-list/contragent-list"
import ContragentEditorWindow from "./contragent-editor/contragent-editor-window"
import ContragentInfoWindow from "./contragent-info/contragent-info-window"
import Counterparty from "../models/Counterparty"
import Employee from "../models/Employee"
import OrgType from "../models/OrgType"
import counterpartyActionsMap from "../actions/index"

interface IState {
   
}

class App extends React.Component<any, IState> {
    render() {
        const { infoWindowVisible, editWindowVisible } = this.props.appState;
        return (
            <div>
                {
                    infoWindowVisible
                        ? <ContragentInfoWindow />
                        : null
                }
                {
                    editWindowVisible
                        ? <ContragentEditorWindow />
                        : null
                }
                <ContragentList />
            </div>
        );
    }
}

function mapStateToProps (state: any) {
  return {
    appState: state
  }
}

function mapDispatchToProps(dispatch: any) {  
  return {
    todoActions: bindActionCreators(counterpartyActionsMap, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);