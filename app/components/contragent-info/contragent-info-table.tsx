import * as React from "react";
import "./contragent-info-table.css";

interface IState {
    title: string;
}

class ContragentInfoTable extends React.Component<any, IState> {

    render() {
        return (
            <table className="table table-borderless">
                <tbody>
                    <tr>
                        <th>Юредический адрес:</th>
                        <td>Смоленская обл. Севастопольская, дом №35</td>
                    </tr>
                    <tr>
                        <th>Телефон:</th>
                        <td>89846543135</td>
                    </tr>
                    <tr>
                        <th>Адрес для рекламной продукции:</th>
                        <td>Тольятти, Северная, дом №16, корпус 3</td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>Alhimov@ya.ru</td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export default ContragentInfoTable;