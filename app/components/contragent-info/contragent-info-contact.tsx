import * as React from "react";
import "./contragent-info-contact.css";

interface IState {
    title: string;
}

class ContragentInfoContact extends React.Component<any, IState> {

    render() {
        return (
            <div className="contact-plate">
                Алхимов Андрей Андреевич<br />
                8921321321<br />
                mdk@ya.ru<br />
            </div>
        );
    }
}

export default ContragentInfoContact;