import * as React from "react";

interface IState {
    title: string;
}

class ContragentInfoPanel extends React.Component<any, IState> {

    render() {
        return (
            <div className="well">
                ОАО "Алхимов А.А."<br />
                Юредическое лицо: ИП<br />
                ИНН: 0461111101<br />
            </div>
        );
    }
}

export default ContragentInfoPanel;